# AMG: Atomic based Method for Gridless 🎻


## Done

* Fast implementation;
* `Charge2D`, a `Curves2D` class wrapper;
* convex and non-convex steps implementation.

## Goals

* support estimation;
* better organised [`offgrid`](https://gitlab.inria.fr/blaville/off-the-grid-python) package: amg subpackage, spikes subpackage, etc.;
* synthetic tests, real data tests;
* Fourier, Laplace, covariance implementation;
* non-square domain??
* full CUDA support;
* a notebook with various examples (loops, really close curves, interweaving curves, etc.);
* comprehensive documentation

## Miscellaneous remarks

* do not use LBFGS for the non-convex step, it is way to slow;
* CUDA is partially supported, ought to clear up the mess in the code (dev=device...);
* amplitude is a bit overestimated: why?

## Experimental tests ressources

* [this ULM paper](https://www.nature.com/articles/s41598-020-62898-9)


## Contribute ![(git logo)](https://yousername.gitlab.io/img/logo_git_50x50.png) 

Maintainer: Bastien Laville (bastien.laville@inria.fr).

Contributors: Bastien Laville, Romain Petit.

Distribution and License: Inria, [CeCILL](http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html) 

Upstream git repository: https://gitlab.inria.fr/blaville/off-the-grid-python
