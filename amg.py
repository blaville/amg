#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Atomic based Method for Gridless (AMG)
Module for curves measures and charges reconstruction

See the following papers:

[1] Bastien Laville, Laure Blanc-Féraud, Gilles Aubert. 
Off-the-grid curve reconstruction through divergence regularisation: an 
extreme point result. SIIMS, SIAM. January 2023.
https://hal.archives-ouvertes.fr/hal-03658949

[2] Bastien Laville, Laure Blanc-Féraud, Gilles Aubert.
Off-the-grid algorithm for curve reconstruction.
SSVM forthcoming. May 2023


Created on Tue May 31 14:36:11 2022

@author: Bastien (https://github.com/XeBasTeX, 
                  https://gitlab.inria.fr/blaville)
"""

import offgrid


import torch
import numpy as np

from scipy.interpolate import splprep, splev
from skimage.measure import label, subdivide_polygon


import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from mpl_toolkits.axes_grid1 import make_axes_locatable

import time
from tqdm import tqdm

import os
os.chdir(os.path.dirname(os.path.abspath(__file__)))


# GPU acceleration if needed
device = "cuda" if torch.cuda.is_available() else "cpu"
device = "cpu"
print("[AMG] Using {} device".format(device))

# Random seed
torch.manual_seed(853)

__tic__ = time.time()


class Curve2D:
    def __init__(self, amplitude=None, position=None, dev=device):
        if amplitude is None or position is None:
            amplitude = torch.Tensor().to(dev)
            position = torch.Tensor().to(dev)
        elif((isinstance(amplitude, float) or isinstance(amplitude, int))
             and amplitude != 0):
            # amplitude = amplitude * torch.ones(len(position) - 1)
            self.a = amplitude
            if isinstance(position, torch.Tensor):
                self.x = position.to(dev)
            if isinstance(position, np.ndarray):
                self.x = torch.from_numpy(position).to(dev)
            if isinstance(position, list):
                self.x = torch.tensor(position).to(dev)
        elif(isinstance(amplitude, torch.Tensor) and amplitude.numel() == 1):
            self.a = amplitude
            if isinstance(position, torch.Tensor):
                self.x = position.to(dev)
            if isinstance(position, np.ndarray):
                self.x = torch.from_numpy(position).to(dev)
            if isinstance(position, list):
                self.x = torch.tensor(position).to(dev)
        else:
            raise TypeError("You provided an inappropriate tensor.")
        self.N_approx = position.shape[0]
        self.device = torch.device(dev)

# There is no self.N because we need to create a superclass of curves that
# encompasses the Curve2D class

    def __add__(self, m, dev=device):
        return Charge2D(np.array([self, m]), dev=dev)

    def __mul__(self, coeff):
        if isinstance(coeff, float) or isinstance(coeff, int):
            return Curve2D(coeff * self.a, self.x)
        else:
            raise TypeError("Illegal multiplication")

    def __truediv__(self, coeff):
        return self.__mul__(1/coeff)

    def __eq__(self, m):
        if m == 0 and (self.a == [] and self.x == []):
            return True
        if isinstance(m, self.__class__):
            return self.__dict__ == m.__dict__
        return False

    def __ne__(self, m):
        return not self.__eq__(m)

    def __str__(self):
        return(f"𝛾(0) = {self.x[0]} and 𝛾(1) = {self.x[-1]}")

    def tv_2(self):
        length = 0
        x = self.x
        for t in range(self.x.shape[0]-1):
            length += torch.linalg.norm(x[t+1]-x[t], 2)
        return self.a * float(length)

    def is_closed(self, tol=5e-2):
        return bool(torch.linalg.norm(self.x[0] - self.x[-1]) < tol)

    def div_tv(self):
        if self.is_closed():
            return 0
        else:
            return float(2 * self.a)

    def norm_V(self):
        return self.tv_2() + self.div_tv()

    def energy(self, dom, noyau, acquis, regul, bruits='gauss'):
        r"""
        Energy of the measure for the Covenant problem
        :math:`(\mathcal{Q}_\lambda (y))` or BLASSO on
        the average acquisition :math:`(\mathcal{P}_\lambda (\overline{y}))`.

        Parameters
        ----------
        dom: :py:class:`offgrid.Domain2D`
            Domain where the acquisition in :math:`\mathrm{L}^2(\mathcal{X})` 
            of :math:`m` lives.
        acquis: Tensor
            Vector of the observation, will be compared with the action of the
            operator on the measure :math:`m`.
        regul : double, optional
            Regularisation parameter `\lambda`.
        obj : str, optional
            Either 'covar' to reconstruct wrt covariance either 'acquis'
            to recostruct wrt to the temporal mean. The default is 'covar'.
        bruits: str, optional
            The object contains all the values defining the noise to be 
            simulated.


        Raises
        ------
        NameError
            The kernel is not recognised by the function.

        Returns
        -------
        double
            Evaluation of :math:`T_\lambda` energy for :math:`m` measurement.

        """
        # normalis = torch.numel(acquis)
        normalis = 1
        if bruits == 'poisson':
            if noyau.type == 'double_gaussian':
                R_nrj = self.cov_kernel(dom, noyau)
                attache = 0.5 * torch.linalg.norm(acquis - R_nrj)**2 / normalis
                parcimonie = regul*self.norm_V()
                return attache + parcimonie
            if noyau.type in ['gaussian', 'gaussian_derivative']:
                simul = self.kernel(dom, noyau)
                attache = 0.5 * torch.linalg.norm(acquis - simul)**2 / normalis
                parcimonie = regul*self.norm_V()
                return attache + parcimonie
        elif bruits in ('gauss', 'unif'):
            if noyau.type == 'double_gaussian':
                R_nrj = self.cov_kernel(dom, noyau)
                attache = 0.5 * torch.linalg.norm(acquis - R_nrj)**2 / normalis
                parcimonie = regul*self.norm_V()
                return attache + parcimonie
            if noyau.type in ['gaussian', 'gaussian_derivative']:
                simul = self.kernel(dom, noyau)
                attache = 0.5 * torch.linalg.norm(acquis - simul)**2 / normalis
                parcimonie = regul*self.norm_V()
                return attache + parcimonie
            raise NameError("Unknown kernel provided.")
        raise NameError("Unknown noise provided.")

    def to(self, dev):
        """
        Sends the Measure2D object to the `device` component (the processor or 
        the Nvidia graphics card)

        Parameters
        ----------
        dev : str
            Either `cpu`, `cuda` (default GPU) or `cuda:0`, `cuda:1`, etc.

        Returns
        -------
        None.

        """
        return Curve2D(self.a, self.x, dev=dev)

    def save(self, path='saved_objects/measure.pt'):
        """
        Save the measure in the given `path` file

        Parameters
        ----------
        path : str, optional
            Path and name of the saved measure object. The default is 
            'saved_objects/measure.pt'.

        Returns
        -------
        None.

        """
        torch.save(self, path)

    def kernel(self, dom, kernel, dev=device):
        N_approx = self.N_approx
        x = self.x
        a = self.a
        if kernel.type == 'gaussian':
            if dev == 'cuda':
                acquis = torch.cuda.FloatTensor(dom.X.shape).fill_(0)
            else:
                acquis = torch.zeros(dom.X.shape)
            sigma = kernel.sigma_psf
            for t in range(N_approx-1):
                x1, x2 = x[t], x[t+1]
                length = torch.linalg.norm(x2 - x1)
                midpoint = (x1 + x2) / 2

                discrepancies_x = (dom.X - midpoint[0])**2
                discrepancies_y = (dom.Y - midpoint[1])**2
                discrep = discrepancies_x + discrepancies_y

                expo = torch.exp(- discrep / (2*sigma**2))
                acquis += length * expo
            return a * acquis
        if kernel.type == 'gaussian_derivative':
            if dev == 'cuda':
                acquis_x = torch.cuda.FloatTensor(dom.X.shape).fill_(0)
                acquis_y = torch.cuda.FloatTensor(dom.X.shape).fill_(0)
            else:
                acquis_x = torch.zeros(dom.X.shape)
                acquis_y = torch.zeros(dom.X.shape)
            sigma = kernel.sigma_psf
            for t in range(N_approx-1):
                x1, x2 = x[t], x[t+1]
                length = torch.linalg.norm(x2 - x1)
                midpoint = (x1 + x2) / 2

                discrepancies_x = dom.X - midpoint[0]
                discrepancies_y = dom.Y - midpoint[1]

                expo_x = offgrid.grad_x_gaussian_2D(discrepancies_x,
                                                    discrepancies_y,
                                                    discrepancies_x,
                                                    sigma)
                expo_y = offgrid.grad_y_gaussian_2D(discrepancies_x,
                                                    discrepancies_y,
                                                    discrepancies_y,
                                                    sigma)
                acquis_x += length * expo_y
                acquis_y += length * expo_x
            acquis = torch.stack((acquis_y, acquis_x), dim=0)
            return a * acquis
        raise NameError("Unknown kernel.")

    def slow_kernel(self, dom, kernel, dev=device):
        N_approx = self.N_approx
        x = self.x
        a = self.a
        if kernel.type == 'gaussian':
            if dev == 'cuda':
                acquis = torch.cuda.FloatTensor(dom.X.shape).fill_(0)
            else:
                acquis = torch.zeros(dom.X.shape)
            sigma = kernel.sigma_psf
            for i in range(acquis.shape[0]):
                for j in range(acquis.shape[1]):
                    pixel = torch.tensor([dom.X[i, j], dom.Y[i, j]]).to(dev)
                    for t in range(N_approx-1):
                        x1, x2 = x[t], x[t+1]
                        length = torch.linalg.norm(x2 - x1)
                        midpoint = (x1 + x2) / 2
                        norm = torch.linalg.norm(pixel - midpoint)**2
                        val = length * torch.exp(- norm / (2*sigma**2))
                        acquis[i, j] += val
            return a * acquis
        if kernel.type == 'gaussian_derivative':
            if dev == 'cuda':
                acquis_x = torch.cuda.FloatTensor(dom.X.shape).fill_(0)
                acquis_y = torch.cuda.FloatTensor(dom.X.shape).fill_(0)
            else:
                acquis_x = torch.zeros(dom.X.shape)
                acquis_y = torch.zeros(dom.X.shape)
            sigma = kernel.sigma_psf
            for i in range(acquis_x.shape[0]):
                for j in range(acquis_x.shape[1]):
                    pixel = torch.tensor([dom.X[i, j], dom.Y[i, j]]).to(dev)
                    for t in range(N_approx-1):
                        x1, x2 = x[t], x[t+1]
                        length = torch.linalg.norm(x2 - x1)
                        midpoint = (x1 + x2) / 2
                        diff = pixel - midpoint
                        coeff = torch.exp(- torch.linalg.norm(diff)
                                          ** 2 / (2*sigma**2))
                        coeff_der = - length * diff / (sigma**2) * coeff
                        acquis_x[i, j] += coeff_der[1]
                        acquis_y[i, j] += coeff_der[0]
            acquis = torch.stack((acquis_y, acquis_x), dim=0)
            return a * acquis
        if kernel.type == 'fourier':
            raise TypeError("Not implemented.")
        if kernel.type == 'laplace':
            raise TypeError("Not implemented.")
        raise NameError("Unknown kernel.")


class Charge2D:
    def __init__(self, curves=None, dev=device):
        # if amplitude is None or curves is None:
        #     amplitude = torch.Tensor().to(dev)
        #     curves = np.array([], dtype=object)
        # assert(len(amplitude) == len(curves)
        #        or len(amplitude) == len(curves))
        # if isinstance(amplitude, torch.Tensor) and isinstance(curves,
        #                                                       np.ndarray):
        #     self.a = amplitude.to(dev)
        #     self.mu = np.array(curves)
        # elif isinstance(amplitude, np.ndarray) and isinstance(curves,
        #                                                       np.ndarray):
        #     self.a = torch.from_numpy(amplitude).to(dev)
        #     self.mu = torch.from_numpy(curves).to(dev)
        # elif isinstance(amplitude, list) and isinstance(curves, list):
        #     self.a = torch.tensor(amplitude).to(dev)
        #     self.mu = np.array(curves)
        # else:
        #     raise TypeError("Please check the format of your input")
        if curves is None:
            curves = np.array([], dtype=object)
        self.curves = curves
        self.N = len(curves)
        self.device = torch.device(dev)

    def __add__(self, m, dev=device):
        if self.N == 0:
            return m
        if m.N == 0:
            return self
        curves_new = np.append(m.curves, self.curves)
        return Charge2D(curves_new, dev=dev)

    def __mul__(self, coeff):
        if isinstance(coeff, float):
            result = Charge2D()
            for i in range(self.N):
                result += coeff * self[i]
        else:
            raise TypeError("Illegal multiplication")

    def __truediv__(self, coeff):
        return self.__mul__(1/coeff)

    def __eq__(self, m):
        # Hierher debug
        if m == 0 and (self.curves == [] and self.curves == []):
            return True
        if isinstance(m, self.__class__):
            return self.__dict__ == m.__dict__
        return False

    def __ne__(self, m):
        return not self.__eq__(m)

    def __getitem__(self, items):
        return self.curves[items]

    def __str__(self):
        if self.N == 0:
            return("Empty charge")
        return(f"{self.N} curves with amplitudes: {self.curves_amplitudes()}")

    def curves_amplitudes(self):
        """
        Output the concatenated amplitudes of the curves in the `self` Charge2D

        Returns
        -------
        amp : Tensor
            Amplitudes weighting all the curves lying in the `self` object.

        """
        amp = torch.tensor([]).to(self.device)
        for i in range(self.N):
            tmp = torch.tensor([self.curves[i].a]).to(self.device)
            amp = torch.cat((amp, tmp))
        return amp

    def curves_positions(self):
        """
        Output the concatenated positions of the curves in the `self` Charge2D

        Returns
        -------
        pos : Tensor
            Positions describing all the curves lying in the `self` object.

        """
        pos = torch.tensor([]).to(self.device)
        for i in range(self.N):
            pos = torch.cat((pos, self.curves[i].x))
        return pos

    def curves_separated_positions(self):
        """
        Output the concatenated amplitudes of the curves in the `self` Charge2D

        Returns
        -------
        amp : Tensor
            Amplitudes weighting all the curves lying in the `self` object.

        """
        pos = []
        for i in range(self.N):
            pos += [self.curves[i].x]
        return pos

    def to(self, dev):
        """
        Sends the Measure2D object to the `device` component (the processor or 
        the Nvidia graphics card)

        Parameters
        ----------
        dev : str
            Either `cpu`, `cuda` (default GPU) or `cuda:0`, `cuda:1`, etc.

        Returns
        -------
        None.

        """
        return Charge2D(self.curves, dev=dev)

    def kernel(self, dom, noyau, dev=device):
        r"""
        Applies a kernel to the charge :math:`m`.
        Supported: convolution with Gaussian kernel.

        Parameters
        ----------
        dom: :py:class:`offgrid.Domain2D`
            Domain where the acquisition in :math:`\mathrm{L}^2(\mathcal{X})` 
            of :math:`m` lives.
        kernel: str, optional
            Class of the kernel applied to the measurement. Only the classes
            'gaussian' and 'fourier' are currently supported, the Laplace 
            kernel or the Laplace kernel or the Airy function will probably be 
            implemented in a future version. The default is 'gaussian'.

        Raises
        ------
        TypeError
            The kernel is not yet implemented.
        NameError
            The kernel is not recognised.

        Returns
        -------
        acquired: Tensor
            Matrix discretizing :math:`\Phi(m)` .

        """

        N = self.N
        curves = self.curves
        X_domain = dom.X
        if noyau.type == 'gaussian':
            if dev == 'cuda':
                acquis = torch.cuda.FloatTensor(X_domain.shape).fill_(0)
            else:
                acquis = torch.zeros(X_domain.shape)
            for i in range(0, N):
                acquis += curves[i].kernel(dom, noyau, dev=dev)
            return acquis
        if noyau.type == 'gaussian_derivative':
            if dev == 'cuda':
                acquis = torch.cuda.FloatTensor(2, X_domain.shape[0],
                                                X_domain.shape[1]).fill_(0)
            else:
                acquis = torch.zeros(2, X_domain.shape[0], X_domain.shape[1])
            for i in range(0, N):
                acquis += curves[i].kernel(dom, noyau, dev=dev)
            return acquis
        if noyau.type == 'double_gaussian':
            raise TypeError("Not implemented.")
        if noyau.type == 'fourier':
            raise TypeError("Not implemented.")
        if noyau.type == 'laplace':
            raise TypeError("Not implemented.")
        raise NameError("Unknown kernel.")

    def tv_2(self):
        tv2 = 0
        for i in range(self.N):
            tv2 += self.curves[i].tv_2()
        return tv2

    def div_tv(self):
        div = 0
        for i in range(self.N):
            div += self.curves[i].div_tv()
        return div

    def norm_V(self):
        return self.tv_2() + self.div_tv()

    def energy(self, dom, noyau, acquis, regul, bruits='gauss'):
        r"""
        Energy of the measure for the Covenant problem
        :math:`(\mathcal{Q}_\lambda (y))` or BLASSO on
        the average acquisition :math:`(\mathcal{P}_\lambda (\overline{y}))`.

        Parameters
        ----------
        dom: :py:class:`offgrid.Domain2D`
            Domain where the acquisition in :math:`\mathrm{L}^2(\mathcal{X})` 
            of :math:`m` lives.
        acquis: Tensor
            Vector of the observation, will be compared with the action of the
            operator on the measure :math:`m`.
        regul : double, optional
            Regularisation parameter `\lambda`.
        obj : str, optional
            Either 'covar' to reconstruct wrt covariance either 'acquis'
            to recostruct wrt to the temporal mean. The default is 'covar'.
        bruits: str, optional
            The object contains all the values defining the noise to be 
            simulated.


        Raises
        ------
        NameError
            The kernel is not recognised by the function.

        Returns
        -------
        double
            Evaluation of :math:`T_\alpha` energy for :math:`m` measurement.

        """
        nrj = 0
        for i in range(self.N):
            nrj += self.curves[i].energy(dom, noyau, acquis, regul, bruits)
        return nrj

    def plot(self, acquis, dom, title=None):
        plt.figure()
        cont1 = plt.contourf(dom.X.to('cpu'), dom.Y.to('cpu'),
                             acquis.to('cpu'), 100, cmap='bone')
        for c in cont1.collections:
            c.set_edgecolor("face")

        for i in range(self.N):
            charge_cpu = self[i].to('cpu')
            if i == 0:
                plt.plot(charge_cpu.x[:, 0], charge_cpu.x[:, 1], c='pink',
                         label='Charge support')
            else:
                plt.plot(charge_cpu.x[:, 0], charge_cpu.x[:, 1], c='pink')
            plt.scatter(charge_cpu.x[0, 0], charge_cpu.x[0, 1], c='pink')
            plt.scatter(charge_cpu.x[-1, 0], charge_cpu.x[-1, 1], c='pink')

        plt.xlabel('$x$', fontsize=18)
        plt.ylabel('$y$', fontsize=18)
        plt.axis('square')
        # plt.title('Charge',fontsize=18)
        plt.axis([dom.x_gauche, dom.x_droit, dom.x_gauche, dom.x_droit])
        plt.legend()
        if title is not None:
            plt.savefig("fig/csfw/" + title + ".pdf",
                        format='pdf', dpi=1000, bbox_inches='tight',
                        pad_inches=0.03)
        plt.show()
        return

    def save(self, path='saved_objects/charge.pt'):
        """
        Save the measure in the given `path` file

        Parameters
        ----------
        path : str, optional
            Path and name of the saved measure object. The default is 
            'saved_objects/charge.pt'.

        Returns
        -------
        None.

        """
        torch.save(self, path)

    def prune(self, tol=1e-4):
        r"""
        Discard the curves with very low amplitudes 
        (can be understood as numerical artifacts).

        Parameters
        ----------
        tol : double, optional
            Tolerance below which curves are not kept. The default is 
            1e-3.

        Returns
        -------
        m : Charge2D
            Discrete measures without the low-amplitudes curves.


        """
        # nnz = np.count_nonzero(self.a)
        nnz_a = self.curves.a.clone().detach()
        nnz = nnz_a > tol
        nnz_a = nnz_a[nnz]
        nnz_x = self.curves.x.clone().detach()
        nnz_x = self.curves
        raise TypeError("Hierher implement")
        m = Charge2D(nnz_a, nnz_x, dev=device)
        return m


def phiCurve(m, dom, noyau):
    r"""
    Compute the output of an acquisition operator with charge :math:`m` as 
    an input

    Parameters
    ----------
    m : Curves2D
        Curve measure on :math:`\mathcal{X}`.
    dom : Domain2D
        Domain :math:`\mathcal{X}` on which the acquisition of :math:`m` 
        is computed in :math:`\mathrm{L}^2(\mathcal{X})` if the objective is 
        acquisition or :math:`\mathrm{L}^2(\mathcal{X^2})` if the objective is
        covariance.
    noyau : Kernel2D
        Provides the forward operator.

    Raises
    ------
    TypeError
        The acquisition operator (or rather its kernel) is not recognised.

    Returns
    -------
    Tensor
        Returns :math:`\Phi(m)` if the objective is acquisition, and
        :math:`\Lambda(m)` if the objective is covariance.

    """
    if noyau.type in ['gaussian', 'gaussian_derivative', 'fourier']:
        return m.kernel(dom, noyau)
    if noyau.type == 'double_gaussian':
        return m.cov_kernel(dom, noyau)
    if noyau.type == 'laplace':
        raise NameError('Laplace is not implemented yet.')
    raise NameError('Unknown CROC target.')


def phi_charge_2D(a, curves, dom, noyau):
    r"""
    Compute the output image of the forward operator evaluated on the discrete
    measure :math:`m_{a,x}` with amplitude a and position x as an input.

    Parameters
    ----------
    a : array_like
        Luminosity vector, ought to have the same number of elements as 
        :math:`x`.
    x : array_like
        Luminosity vector, ought to have the same number of elements as 
        :math:`a`.
    dom : Domain2D
        Domain :math:`\mathcal{X}` on which the acquisition of :math:`m` 
        is computed in :math:`\mathrm{L}^2(\mathcal{X})` if the objective is 
        acquisition or :math:`\mathrm{L}^2(\mathcal{X^2})` if the objective is
        covariance.
    obj : str, optional
        Provide the objective of the operator. The default is 'covar'.

    Raises
    ------
    TypeError
        The operator's objective (or rather its kernel) is not recognised.

    Returns
    -------
    Tensor
        Returns :math:`\Phi(m)` if the objective is acquisition, and
        :math:`\Lambda(m)` if the objective is covariance.

    """
    if noyau.type in ['gaussian', 'gaussian_derivative', 'fourier']:
        m_tmp = Charge2D(a, curves)
        return m_tmp.kernel(dom, noyau)
    if noyau.type == 'double_gaussian':
        raise TypeError('Not implemented.')
    raise TypeError('Unknown kernel.')


def phiStar(acquis_x, acquis_y, dom, kernel):
    r"""
    Adjoint operator :math:`\Phi^\ast` evaluated at a point :math:`y`.

    Parameters
    ----------
    acquis_x : TYPE
        DESCRIPTION.
    acquis_y : TYPE
        DESCRIPTION.
    dom : TYPE
        DESCRIPTION.
    kernel : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    N_ech = dom.N_ech
    sigma = kernel.sigma_psf
    (X_big, Y_big) = dom.big()
    h_vec_x = offgrid.grad_x_gaussian_2D(X_big, Y_big, X_big, sigma)
    h_vec_y = offgrid.grad_y_gaussian_2D(X_big, Y_big, Y_big, sigma)
    h_ker_x = h_vec_x.reshape(1, 1, N_ech*2-1, N_ech*2-1)
    h_ker_y = h_vec_y.reshape(1, 1, N_ech*2-1, N_ech*2-1)
    y_arr_x = acquis_x.reshape(1, 1, N_ech, N_ech)
    y_arr_y = acquis_y.reshape(1, 1, N_ech, N_ech)
    eta_x = torch.nn.functional.conv2d(h_ker_x, y_arr_x, stride=1)
    eta_y = torch.nn.functional.conv2d(h_ker_y, y_arr_y, stride=1)
    eta_x = torch.flip(torch.squeeze(eta_x), [1, 0]) / N_ech**2
    eta_y = torch.flip(torch.squeeze(eta_y), [1, 0]) / N_ech**2
    phi_ast = torch.stack((eta_x, eta_y), dim=0)
    return phi_ast


def etakCurve(measure, acquis, dom, noyau, regul):
    r"""Dual certificate :math:`\eta_\alpha` associated to the charge
    :math:`m_\alpha`. The certificate gives the approximation
    (on the grid) of the curve measure with strongest intensity 
    in the residual.
    

    Parameters
    ----------
    measure : Curves2D
        Curve measure on :math:`\mathcal{X}`.
    acquis : TYPE
        DESCRIPTION.
    dom : Domain2D
        Domain :math:`\mathcal{X}` on which the acquisition of :math:`m` 
        is computed in :math:`\mathrm{L}^2(\mathcal{X})` if the objective is 
        acquisition or :math:`\mathrm{L}^2(\mathcal{X^2})` if the objective is
        covariance.
    noyau : Kernel2D
        Provides the forward operator.
    regul : double, optional
        Regularisation parameter `\alpha`.

    Returns
    -------
    None.

    """
    residual = acquis - phiCurve(measure, dom, noyau)
    eta = 1/regul*phiStar(residual[0], residual[1], dom, noyau)
    return eta


def phiCharge(c, dom, noyau):
    r"""
    Compute the output of an acquisition operator with charge :math:`m` as 
    an input

    Parameters
    ----------
    c : Charge2D
        Divergence vector field on :math:`\mathcal{X}`.
    dom : Domain2D
        Domain :math:`\mathcal{X}` on which the acquisition of :math:`m` 
        is computed in :math:`\mathrm{L}^2(\mathcal{X})` if the objective is 
        acquisition or :math:`\mathrm{L}^2(\mathcal{X^2})` if the objective is
        covariance.
    noyau : Kernel2D
        Provides the forward operator.

    Raises
    ------
    TypeError
        The acquisition operator (or rather its kernel) is not recognised.

    Returns
    -------
    Tensor
        Returns :math:`\Phi(m)` if the objective is acquisition, and
        :math:`\Lambda(m)` if the objective is covariance.

    """
    if len(c.curves) == 0:
        return 0
    acquis = c.curves[0].kernel(dom, noyau)
    for i in range(1, c.N):
        acquis += c.curves[i].kernel(dom, noyau)
    return acquis


def phi_amplitude_charge_2D(a, x, dom, noyau, dev=device):
    assert(len(a) == len(x))
    if len(x) == 0:
        return 0
    curve_tmp = Curve2D(1, x[0])  # careful! x is a list of 2D tensor
    ker = a[0] * curve_tmp.kernel(dom, noyau, dev=dev)
    for i in range(1, len(a)):
        curve_tmp = Curve2D(1, x[i])  # careful! x is a list of 2D tensor
        ker += a[i] * curve_tmp.kernel(dom, noyau, dev=dev)
    return ker


def etakCharge(charge, acquis, dom, noyau, regul):
    r"""Dual certificate :math:`\eta_\alpha` associated to the charge
    :math:`m_\alpha`. The certificate gives the approximation
    (on the grid) of the curve measure with strongest intensity 
    in the residual.
    

    Parameters
    ----------
    charge : Charge2D
        Divergence vector field on :math:`\mathcal{X}`.
    acquis : TYPE
        DESCRIPTION.
    dom : Domain2D
        Domain :math:`\mathcal{X}` on which the acquisition of :math:`m` 
        is computed in :math:`\mathrm{L}^2(\mathcal{X})` if the objective is 
        acquisition or :math:`\mathrm{L}^2(\mathcal{X^2})` if the objective is
        covariance.
    noyau : Kernel2D
        Provides the forward operator.
    regul : double, optional
        Regularisation parameter `\alpha`.

    Returns
    -------
    None.

    """
    residual = acquis - phiCharge(charge, dom, noyau)
    eta = 1/regul*phiStar(residual[0], residual[1], dom, noyau)
    return eta


def bridgeTheCurveGap(curve, dev=device):
    pos = curve.x
    if curve.is_closed():
        pos[-1] = pos[0]
    return Curve2D(curve.a, pos, dev=dev)


def bridgeTheChargeGap(charge, dev=device):
    c_res = Charge2D(dev=dev)
    for i in range(charge.N):
        pos = charge[i].x
        if charge[i].is_closed():
            pos[-1] = pos[0]
        c_tmp = Curve2D(charge[i].a, pos, dev=dev)
        c_res = Charge2D(np.append(c_res.curves, c_tmp))
    return c_res


# Curve simulation

# Domain
N_ECH = int(2**6)
X_GAUCHE = 0
X_DROIT = 1
domain = offgrid.Domain2D(X_GAUCHE, X_DROIT, N_ECH, dev=device)
domain_cpu = domain.to('cpu')

SIGMA = 2e-2
psf = offgrid.Kernel2D('gaussian', {'sigma_psf': SIGMA})
psf_grad = offgrid.Kernel2D('gaussian_derivative', {'sigma_psf': SIGMA})

q = 2**2
super_domain = domain.super_resolve(q, SIGMA/4)


# Simulate a curve measure
N_pts_curve = int(N_ECH/3-2)

# Spiral
t_vector = torch.linspace(0.2, 1, 4*N_pts_curve)
x_vector = 0.2 * t_vector * torch.cos(2*np.pi*t_vector) + 0.3
y_vector = 0.2 * t_vector * torch.sin(2*np.pi*t_vector) + 0.3
pos_spiral = torch.stack((x_vector, y_vector), dim=-1)

# Circle
t_vector = torch.linspace(0, 1, 4*N_pts_curve)
x_vector = 0.14 * torch.cos(2*np.pi*t_vector) + 0.7
y_vector = 0.14 * torch.sin(2*np.pi*t_vector) + 0.7
pos_circle = torch.stack((x_vector, y_vector), dim=-1)


# Curve measures
µ_𝛾 = Curve2D(0.5, pos_circle)
mu_1 = µ_𝛾 / µ_𝛾.norm_V()  # normalised charge
mu_1_cpu = mu_1.to('cpu')

µ_𝛾_2 = Curve2D(1.0, pos_spiral)
mu_2 = µ_𝛾_2 / µ_𝛾_2.norm_V()  # multiple of normalised charge
mu_2_cpu = mu_2.to('cpu')

dvf = mu_1 * 4 + mu_2 * 7
y_dvf = dvf.kernel(domain, psf)
# grad_y_dvf = dvf.kernel(domain, psf_grad)

σ_b = 4e-3
y_dvf += torch.normal(0, σ_b, size=y_dvf.shape).to(device)
# grad_y_dvf = torch.stack(torch.gradient(y_dvf))

# grad_y_dvf += torch.normal(0, 1e-0, size=grad_y_dvf.shape).to(device)
grad_y_dvf = torch.tensor(np.gradient(np.array(y_dvf.to('cpu')))).to(device)

plt.imshow(y_dvf.to('cpu').T, origin='lower', cmap='bone')
plt.axis('off')
plt.colorbar()
plt.savefig("fig/image_acquis.pdf", format='pdf', dpi=1000, 
            bbox_inches='tight', pad_inches=0.03)


# certif = etakCharge(dvf, 0, domain, psf_grad, 1)
fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 5))
im1 = axes.flat[0].imshow(grad_y_dvf[0].to('cpu').T, origin='lower',
                          cmap='bone')
axes.flat[1].imshow(grad_y_dvf[1].to('cpu').T, origin='lower', cmap='bone')
axes[0].axis('off')
axes[1].axis('off')
axes.flat[0].title.set_text(r'$(\nabla y)^{(1)}$')
axes.flat[1].title.set_text(r'$(\nabla y)^{(2)}$')
# fig.subplots_adjust(right=0.78)
# cbar_ax = fig.add_axes([0.85, 0.15, 0.03, 0.7])
# fig.colorbar(im1, cax=cbar_ax)
plt.savefig("fig/image_grad_acquis.pdf", format='pdf', dpi=1000,
            bbox_inches='tight', pad_inches=0.03)
plt.show()


# pos_cannele = torch.tensor([[-0.7562075, -0.52150526],
#                             [-0.72804763, -0.51921652],
#                             [-0.69831906, -0.51230641],
#                             [-0.66710537, -0.50108241],
#                             [-0.63449013, -0.48585193],
#                             [-0.60055693, -0.46692243],
#                             [-0.56538936, -0.44460135],
#                             [-0.52907099, -0.41919613],
#                             [-0.49168541, -0.39101422],
#                             [-0.4533162, -0.36036306],
#                             [-0.41404694, -0.32755009],
#                             [-0.37396122, -0.29288275],
#                             [-0.33314262, -0.25666848],
#                             [-0.29167471, -0.21921474],
#                             [-0.24964109, -0.18082896],
#                             [-0.20712533, -0.14181858],
#                             [-0.16421102, -0.10249105],
#                             [-0.12098173, -0.06315381],
#                             [-0.07752106, -0.0241143],
#                             [-0.03391258,  0.01432003],
#                             [0.00976012,  0.05184174],
#                             [0.05341346,  0.08814339],
#                             [0.09696386,  0.12291753],
#                             [0.14032774,  0.15585673],
#                             [0.18342151,  0.18665354],
#                             [0.22616159,  0.21500051],
#                             [0.26846441,  0.24059021],
#                             [0.31024637,  0.26311519],
#                             [0.35142389,  0.28226801],
#                             [0.3919134,  0.29774123],
#                             [0.43163131,  0.3092274],
#                             [0.47049403,  0.31641907],
#                             [0.50841799,  0.31900882],
#                             [0.5453196,  0.31668919],
#                             [0.58111529,  0.30915274],
#                             [0.61572146,  0.29609204],
#                             [0.64905453,  0.27719962],
#                             [0.68103093,  0.25216807],
#                             [0.71156707,  0.22068992],
#                             [0.74057937,  0.18245774],
#                             [0.76798424,  0.13716408],
#                             [0.79369811,  0.08450151],
#                             [0.81763739,  0.02416258],
#                             [0.8397185, -0.04416016]])

# pos_cannele = (pos_cannele+2.7)/4



# # %% Forward operator (fast)

# y_0 = mu_1.kernel(domain, psf)
# grad_y = mu_1.kernel(domain, psf_grad)

# alpha = 1
# eta_alpha_2 = 3*etakCurve(mu_1, 0, domain, psf_grad, alpha) + \
#     1.4*etakCurve(mu_2, 0, domain, psf_grad, alpha)

# eta_alpha = etakCurve(mu_1, 0, domain, psf_grad, alpha) + \
#     5*etakCurve(mu_2, 0, domain, psf_grad, alpha)

# #%% Plotting

# # # Curve blurred
# # plt.figure()
# # cont1 = plt.contourf(domain_cpu.X, domain_cpu.Y, y_0.to('cpu'), 100,
# #                      cmap='bone')
# # for c in cont1.collections:
# #     c.set_edgecolor("face")
# # plt.colorbar()
# # plt.plot(mu_1_cpu.x[:, 0], mu_1_cpu.x[:, 1],
# #          c='red', label='$\Gamma=\gamma([0,1])$')
# # plt.scatter(mu_1_cpu.x[0, 0], mu_1_cpu.x[0, 1], c='red')
# # plt.scatter(mu_1_cpu.x[-1, 0], mu_1_cpu.x[-1, 1], c='red')
# # plt.xlabel('$x$', fontsize=18)
# # plt.ylabel('$y$', fontsize=18)
# # plt.title('Measure $\mu_\gamma$ and its acquisition on $\mathcal{X}$',
# #           fontsize=18)
# # plt.axis([0, 1, 0, 1])
# # plt.legend()


# # # Gradient of the curve
# # y_0_np = y_0.cpu().detach().numpy()
# # nabla_y = np.gradient(y_0_np, 1/(N_ECH), edge_order=2)


# # fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 5))
# # im1 = axes.flat[0].imshow(nabla_y[0].T, origin='lower', cmap='bone')
# # axes.flat[0].title.set_text(r'$(\nabla y)^{(1)}$')
# # axes.flat[0].scatter(N_ECH*mu_1_cpu.x[0, 0], N_ECH*mu_1_cpu.x[0, 1], c='red')
# # axes.flat[0].scatter(N_ECH*mu_1_cpu.x[-1, 0], N_ECH*mu_1_cpu.x[-1, 1], c='red')
# # axes.flat[0].plot(N_ECH*(mu_1_cpu.x[:, 0]), N_ECH*(mu_1_cpu.x[:, 1]),
# #                   c='red', label='$\Gamma=\gamma([0,1])$')
# # im2 = axes.flat[1].imshow(nabla_y[1].T, origin='lower', cmap='bone')
# # axes.flat[1].title.set_text(r'$(\nabla y)^{(2)}$')
# # axes.flat[1].scatter(N_ECH*mu_1_cpu.x[0, 0], N_ECH*mu_1_cpu.x[0, 1], c='red')
# # axes.flat[1].scatter(N_ECH*mu_1_cpu.x[-1, 0], N_ECH*mu_1_cpu.x[-1, 1], c='red')
# # axes.flat[1].plot(N_ECH*(mu_1_cpu.x[:, 0]), N_ECH*(mu_1_cpu.x[:, 1]),
# #                   c='red', label='$\Gamma=\gamma([0,1])$')
# # axes[0].axis('off')
# # axes[1].axis('off')
# # axes[0].legend()
# # axes[1].legend()
# # fig.subplots_adjust(right=0.78)
# # cbar_ax = fig.add_axes([0.85, 0.15, 0.03, 0.7])
# # fig.colorbar(im1, cax=cbar_ax)
# # plt.show()


# # fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 5))
# # im1 = axes.flat[0].imshow(grad_y[0].T.to('cpu'), origin='lower', cmap='bone')
# # axes.flat[0].title.set_text(r'$(\Phi \mu_\gamma)^{(1)}$')
# # axes.flat[0].scatter(N_ECH*mu_1_cpu.x[0, 0], N_ECH*mu_1_cpu.x[0, 1], c='red')
# # axes.flat[0].scatter(N_ECH*mu_1_cpu.x[-1, 0], N_ECH*mu_1_cpu.x[-1, 1], c='red')
# # axes.flat[0].plot(N_ECH*(mu_1_cpu.x[:, 0]), N_ECH*(mu_1_cpu.x[:, 1]),
# #                   c='red', label='$\Gamma=\gamma([0,1])$')
# # im2 = axes.flat[1].imshow(grad_y[1].T.to('cpu'), origin='lower', cmap='bone')
# # axes.flat[1].title.set_text(r'$(\Phi \mu_\gamma)^{(2)}$')
# # axes.flat[1].scatter(N_ECH*mu_1_cpu.x[0, 0], N_ECH*mu_1_cpu.x[0, 1], c='red')
# # axes.flat[1].scatter(N_ECH*mu_1_cpu.x[-1, 0], N_ECH*mu_1_cpu.x[-1, 1], c='red')
# # axes.flat[1].plot(N_ECH*(mu_1_cpu.x[:, 0]), N_ECH*(mu_1_cpu.x[:, 1]),
# #                   c='red', label='$\Gamma=\gamma([0,1])$')
# # axes[0].axis('off')
# # axes[1].axis('off')
# # axes[0].legend()
# # axes[1].legend()
# # fig.subplots_adjust(right=0.78)
# # cbar_ax = fig.add_axes([0.85, 0.15, 0.03, 0.7])
# # fig.colorbar(im1, cax=cbar_ax)
# # plt.show()


# # Laplacian of the curve
# fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 5))
# im1 = axes.flat[0].imshow(eta_alpha[0].T.to('cpu'), origin='lower',
#                           cmap='bone')
# axes.flat[0].title.set_text(r'$(\Phi^\ast y)^{(1)}$')
# axes.flat[0].scatter((N_ECH-1)*dvf_cpu[0].x[0, 0], (N_ECH-1)*dvf_cpu[0].x[0, 1],
#                       c='red')
# axes.flat[0].scatter((N_ECH-1)*dvf_cpu[0].x[-1, 0], (N_ECH-1)*dvf_cpu[0].x[-1, 1],
#                       c='red')
# axes.flat[0].plot((N_ECH-1)*(dvf_cpu[0].x[:, 0]), (N_ECH-1)*(dvf_cpu[0].x[:, 1]),
#                   c='red', label='$\Gamma=\gamma([0,1])$')

# im2 = axes.flat[1].imshow(eta_alpha[1].T.to('cpu'), origin='lower',
#                           cmap='bone')
# axes.flat[1].title.set_text(r'$(\Phi^\ast y)^{(2)}$')
# axes.flat[1].scatter((N_ECH-1)*dvf_cpu[0].x[0, 0],
#                       (N_ECH-1)*dvf_cpu[0].x[0, 1], c='red')
# axes.flat[1].scatter((N_ECH-1)*dvf_cpu[0].x[-1, 0],
#                       (N_ECH-1)*dvf_cpu[0].x[-1, 1], c='red')
# axes.flat[1].plot((N_ECH-1)*(dvf_cpu[0].x[:, 0]), (N_ECH-1)*(dvf_cpu[0].x[:, 1]),
#                   c='red', label='$\Gamma=\gamma([0,1])$')

# axes[0].axis('off')
# axes[1].axis('off')
# axes[0].legend()
# axes[1].legend()

# fig.subplots_adjust(right=0.78)
# cbar_ax = fig.add_axes([0.85, 0.15, 0.03, 0.7])
# fig.colorbar(im1, cax=cbar_ax)
# plt.show()

# # Sum
# lapl = eta_alpha[0].T + eta_alpha[1].T
# plt.imshow(lapl, origin='lower', cmap='bone')
# plt.colorbar()
# plt.scatter((N_ECH-1)*dvf_cpu[0].x[0, 0], (N_ECH-1)*dvf_cpu[0].x[0, 1], c='red')
# plt.scatter((N_ECH-1)*dvf_cpu[0].x[-1, 0], (N_ECH-1)*dvf_cpu[0].x[-1, 1], c='red')
# plt.plot((N_ECH-1)*(dvf_cpu[0].x[:, 0]), (N_ECH-1)*(dvf_cpu[0].x[:, 1]),
#           c='red', label='$\Gamma=\gamma([0,1])$')
# plt.axis('off')
# plt.legend()
# plt.savefig("fig/lapl_sum.pdf", format='pdf', dpi=1000, bbox_inches='tight',
#             pad_inches=0.03)
# plt.show()


# # Threshold
# # arbitrary threshold at this point
# lapl_bool = lapl.to('cpu') < lapl.min() * 60/100
# plt.imshow(lapl_bool, origin='lower', cmap='bone')
# plt.scatter((N_ECH-1)*dvf_cpu[0].x[0, 0], (N_ECH-1)*dvf_cpu[0].x[0, 1], c='red')
# plt.scatter((N_ECH-1)*dvf_cpu[0].x[-1, 0], (N_ECH-1)*dvf_cpu[0].x[-1, 1], c='red')
# plt.plot((N_ECH-1)*(dvf_cpu[0].x[:, 0]), (N_ECH-1)*(dvf_cpu[0].x[:, 1]),
#           c='red', label='$\Gamma=\gamma([0,1])$')
# plt.axis('off')
# plt.legend()
# plt.savefig("fig/support.pdf", format='pdf', dpi=1000, bbox_inches='tight',
#             pad_inches=0.03)
# plt.show()

# # connected_set_count = label(~lapl_bool, return_num=True)[1]
# # print(f"[+] {connected_set_count} connected component(s)")

# # # Threshold
# # # 50 greatest value
# # _, laplou_ind = torch.topk(lapl.to('cpu'), 50, sorted=False)


# # Spline approximation
# # https://stackoverflow.com/questions/31464345/fitting-a-closed-curve-to-a-set-of-points
# # mieux ici https://stackoverflow.com/questions/65580488/fitting-closed-curve-to-a-set-of-noisy-points

# # change for ellipse fitting https://scikit-image.org/docs/stable/auto_examples/edges/plot_polygon.html#sphx-glr-auto-examples-edges-plot-polygon-py

# gamma_curve = torch.stack((domain.X[lapl_bool], domain.Y[lapl_bool]), dim=1)
# gamma_curve = gamma_curve.numpy()

# # Complex angle
# complex_gamma_curve = (gamma_curve - gamma_curve.mean(0))
# gamma_curve_sorted = gamma_curve[np.angle(
#     (complex_gamma_curve[:, 0] + 1j*complex_gamma_curve[:, 1])).argsort()]

# tck, u = splprep(gamma_curve_sorted.T, u=None, s=0.0, per=1, k=1)
# u_new = np.linspace(u.min(), u.max(), 1000)
# x_new, y_new = splev(u_new, tck, der=0)

# plt.figure()
# plt.plot(gamma_curve[:, 0], gamma_curve[:, 1], 'rx', label='Pixels')
# plt.plot(x_new, y_new, 'b--', label='Fitted curve')
# plt.axis('square')
# plt.legend()
# plt.show()

#%%


def sort_by_endpoints(coord, N_sample=0.1):
    endpoint_1 = 0
    endpoint_2 = len(coord[0])

    for i in range(len(coord[:, 0])-1):
        if np.linalg.norm(coord[i] - coord[i+1]) > N_sample:
            endpoint_1 = i
            endpoint_2 = i + 1
            break
    return(np.concatenate((coord[endpoint_1+1:, :], coord[:endpoint_2])))


# open_coord = sort_by_endpoints(gamma_curve_sorted)
# plt.figure()
# plt.plot(open_coord[:-1, 0], open_coord[:-1, 1], 'r--', label='Pixels')
# plt.axis('square')
# plt.legend()
# plt.show()


def get_support_from_image(img, threshold=65/100):
    # arbitrary threshold at this point
    lapl_bool = img.T < img.min() * threshold
    lapl_bool = lapl_bool.to('cpu')
    gamma_curve = torch.stack(
        (domain.X[lapl_bool], domain.Y[lapl_bool]), dim=1)
    gamma_curve = gamma_curve.to('cpu').numpy()
    
    # plt.figure()
    # plt.imshow(lapl_bool)
    # plt.show()

    # Open or closed heuristic
    connected_set_count = label(~lapl_bool, return_num=True)[1]
    # print(f"[+] {connected_set_count} connected component(s)")

    # Open curve case
    if connected_set_count == 1:
        lapl_bool = img.T < img.min() * 65/100
        lapl_bool = lapl_bool.to('cpu')
        # Angle sort
        complex_gamma_curve = (gamma_curve - gamma_curve.mean(0))
        gamma_curve_sorted = gamma_curve[np.angle(
            (complex_gamma_curve[:, 0] + 1j*complex_gamma_curve[:, 1])).argsort()]
        gamma_curve_sorted = sort_by_endpoints(gamma_curve_sorted)

        # gamma_curve_sorted = gamma_curve_sorted[::4,:]
        tck, u = splprep(gamma_curve_sorted.T, u=None,
                         s=N_ECH*5e-5, per=0, k=1, quiet=2)
        u_new = np.linspace(u.min(), u.max(), N_pts_curve)
        x_new, y_new = splev(u_new, tck, der=0)
        # plt.figure()
        # plt.plot(x_new, y_new, 'b--')
        # plt.show()
        return(x_new, y_new)

    # Closed curve case
    elif connected_set_count == 2:
        lapl_bool = img.T < img.min() * 95/100
        lapl_bool = lapl_bool.to('cpu')
        # Angle sort
        complex_gamma_curve = (gamma_curve - gamma_curve.mean(0))
        gamma_curve_sorted = gamma_curve[np.angle(
            (complex_gamma_curve[:, 0] + 1j*complex_gamma_curve[:, 1])).argsort()]

        # gamma_curve_sorted = gamma_curve_sorted[::6,:]
        tck, u = splprep(gamma_curve_sorted.T, u=None,
                         s=N_ECH*1e-5, per=1, k=1, quiet=2)
        u_new = np.linspace(u.min(), u.max(), N_pts_curve)
        x_new, y_new = splev(u_new, tck, der=0)
        # plt.figure()
        # plt.plot(x_new, y_new, 'b--')
        # plt.show()
        return(x_new, y_new)
    else:
        raise TypeError(
            f"Absurd curve estimation with {connected_set_count}" +
            " components. You may want to change the threshold")


# support_1 = get_support_from_image(eta_alpha[0].T + eta_alpha[1].T)
# support_2 = get_support_from_image(eta_alpha_2[0].T + eta_alpha_2[1].T)

# plt.figure()
# plt.plot((N_ECH-1)*(mu_1_cpu.x[:, 0]), (N_ECH-1)*(mu_1_cpu.x[:, 1]),
#          c='red', label='$\Gamma=\gamma([0,1])$')
# plt.plot((N_ECH-1)*(mu_2_cpu.x[:, 0]), (N_ECH-1)*(mu_2_cpu.x[:, 1]),
#          c='red')
# plt.plot((N_ECH-1)*support_1[0], (N_ECH-1)*support_1[1], 'b--')
# plt.plot((N_ECH-1)*support_2[0], (N_ECH-1) *
#          support_2[1], 'b--', label='Fitted curve')
# plt.axis('square')
# plt.show()


#%% CSFW

# i = 0
# x_bruite = dvf[i].x - 2e-2*torch.rand(dvf[i].x.shape).to(device)
# mu_bruite = Curve2D(dvf[i].a, x_bruite)
# mu_bruite = mu_bruite / mu_bruite.norm_V()  # normalised charge
# mu_bruite_cpu = mu_bruite.to('cpu')

# plt.figure()
# cont1 = plt.contourf(domain_cpu.X, domain_cpu.Y, y_dvf.to('cpu'), 100,
#                      cmap='bone')
# for c in cont1.collections:
#     c.set_edgecolor("face")

# dvf_cpu = dvf.to('cpu')
# for i in range(dvf_cpu.N):
#     mu_dvf_cpu = dvf[i].to('cpu')
#     if i == 0:
#         plt.plot(mu_dvf_cpu.x[:, 0], mu_dvf_cpu.x[:, 1], c='green',
#                  label='Ground-truth')
#     else:
#         plt.plot(mu_dvf_cpu.x[:, 0], mu_dvf_cpu.x[:, 1], c='green')
#     plt.scatter(mu_dvf_cpu.x[0, 0], mu_dvf_cpu.x[0, 1], c='green')
#     plt.scatter(mu_dvf_cpu.x[-1, 0], mu_dvf_cpu.x[-1, 1], c='green')

# # Crude certificate
# plt.plot(mu_bruite_cpu.x[:, 0], mu_bruite_cpu.x[:, 1], c='violet',
#          label='Support estimation')
# plt.scatter(mu_bruite_cpu.x[0, 0], mu_bruite_cpu.x[0, 1], c='violet')
# plt.scatter(mu_bruite_cpu.x[-1, 0], mu_bruite_cpu.x[-1, 1], c='violet')
# plt.xlabel('$x$', fontsize=18)
# plt.ylabel('$y$', fontsize=18)
# plt.title('Crude certificate approximation', fontsize=18)
# plt.axis([0, 1, 0, 1])
# plt.legend()
# plt.show()

dvf_cpu = dvf.to('cpu')
plt.figure()
for i in range(dvf_cpu.N):
    mu_dvf_cpu = dvf[i].to('cpu')
    plt.plot(mu_dvf_cpu.x[:, 0], mu_dvf_cpu.x[:, 1], c='red',
             label='Ground-truth')
    plt.scatter(mu_dvf_cpu.x[0, 0], mu_dvf_cpu.x[0, 1], c='red')
    plt.scatter(mu_dvf_cpu.x[-1, 0], mu_dvf_cpu.x[-1, 1], c='red')
plt.axis('square')
plt.axis('off')
plt.savefig("fig/ground_truth.pdf", format='pdf', dpi=1000, 
            bbox_inches='tight', pad_inches=0.03)
plt.show()
# plt.savefig('fig/ground_truth.png', transparent=True)

# global_list_of_charges = []


def CSFW(acquis, dom, noyau, regul=1, nIter=5, dev=device, params={}):
    r"""Charge Sliding Frank-Wolfe algorithm for CROC [1] minimisation, 
    hence curve measure reconstruction.

    .. math:: \mathrm{argmin}_{m \in \mathscr{V}} {S}_\alpha(m) =
        \alpha ||m||_\mathscr{V} +
            \dfrac{1}{2} ||\nabla y - \Phi (m)||^2_2.
                \quad (\mathcal{Q}_\alpha(y))


    Parameters
    ----------
    acquis: Tensor
        Vector of the observation, will be compared with the action of the
        operator on the measure :math:`m`.
    dom: :py:class:`offgrid.Domain2D`
        Domain where the acquisition in :math:`\mathcal{H}` 
        of :math:`m` lives.
    regul : double, optional
        Regularisation parameter `\alpha`. The default is 1e-5.
    nIter : int, optional
        Number of iterations. The default is 5.
    dev : str, optional
        Device for eta computation: cpu, cuda:0, etc. The default is device.

    Output
    -------
    mesure_k : Charge2D
            Last charge reconstructed by CSFW.
    nrj_array : ndarray
                Array of the energy :math:`S_\alpha(m^k)`
                along the iterations.
    reconstructed_charges : ndarray
                Array of the reconstructed charges along the iterations.

    Raises
    ------
    TypeError
        The kernel type of `Kernel2D` is unknown.

    References
    ----------
    [1] Bastien Laville, Laure Blanc-Féraud, Gilles Aubert. 
    Off-the-grid curve reconstruction through divergence regularisation: an 
    extreme point result. 2022.
    https://hal.archives-ouvertes.fr/hal-03658949
    """
    # N_ech = dom.N_ech
    # N_grille = dom.N_ech**2
    # if noyau.type == 'double_gaussian':
    #     N_grille = N_grille**2

    acquis = acquis.to(dev)

    # Parameters checkup
    # mes_init is an initialisation of SFW with a measure
    if 'mes_init' in params:
        mes_init = params['mes_init']
    else:
        mes_init = None
    # mes_init specifies if SFW outputs also the measure reconstructed along
    # the iterations
    if 'mesParIter' in params:
        mesParIter = params['mesParIter']
    else:
        mesParIter = True
    # verbose tunes the log verbosity level (at the moment, On/Off)
    if 'verbose' in params:
        verbose = params['verbose']
    else:
        verbose = False
        # verbose = True # Hierher change when debug is finished

    # Initialisation
    if mes_init == None:
        charge_k = Charge2D(dev=dev)
        a_k = torch.Tensor().to(dev)
        charge_k_demi = Curve2D(dev=dev)
        Nk = 0
    else:
        raise TypeError('Not implemented')
    if mesParIter:
        reconstructed_charges = torch.Tensor()
    nrj_array = torch.zeros(nIter)
    N_vector = [Nk]
    if verbose == True:
        loop = range(nIter)
        print('\n[AMG] CSFW on verbose log')
        print('---- Beginning of the loop ----')
    else:
        loop = tqdm(range(nIter), desc=f'[AMG] Computing CSFW for {dom.N_ech}'
                    + f' x {dom.N_ech} image on {dev}',
                    position=0, leave=True)
    for k in loop:
        if verbose:
            print('\n' + 'Step number ' + str(k))

        eta_V_k = - 1/regul * etakCurve(charge_k, acquis, dom, noyau, regul)
        # plt.figure()
        # plt.imshow(eta_V_k[0].T + eta_V_k[1].T)

        # Stopping condition (Hierher change it)
        if torch.linalg.norm(eta_V_k) < 0:
            nrj_array[k] = charge_k.energie(dom, noyau, acquis, regul)
            if verbose:
                print("\n\n---- Stopping condition ----")
            if mesParIter:
                return(charge_k, nrj_array[:k], reconstructed_charges)
            return(charge_k, nrj_array[:k])

        # Linear step not implemented
        spt = get_support_from_image(eta_V_k[0].T + eta_V_k[1].T)
        gamma_ast = np.dstack((spt[0], spt[1]))[0]
        mu_star = Curve2D(dvf[k].a, gamma_ast)
        mu_star = mu_star / mu_star.norm_V()  # normalised charge

        # # For test only
        # x_bruite = dvf[k].x - 2e-2*torch.rand(dvf[k].x.shape).to(dev)
        # mu_bruite = Curve2D(dvf[k].a, x_bruite)
        # mu_bruite = mu_bruite / mu_bruite.norm_V()  # normalised charge
        # mu_star = mu_bruite

        if mu_star.is_closed():
            mu_star = bridgeTheCurveGap(mu_star)

        if not charge_k.curves_amplitudes().numel():
            charge_k_demi_init = Charge2D(np.array([mu_star]))
            a_param = (torch.ones(Nk+1, dtype=torch.float)
                       ).to(dev).detach().requires_grad_(True)
        else:
            charge_k_demi_init = charge_k + Charge2D(np.array([mu_star]))
            uno = torch.tensor([10.0], dtype=torch.float).to(dev).detach()
            a_param = torch.cat((a_k, uno))
            a_param.requires_grad = True

        # Convex (obviously) LASSO (step 7)
        mse_loss = torch.nn.MSELoss(reduction='sum')
        optimizer = torch.optim.LBFGS([a_param])
        n_epoch = 50
        x_k_flat = charge_k_demi_init.curves_separated_positions()

        for epoch in tqdm(range(n_epoch),
                          desc=f'[AMG] Computing convex step {k} on {dev}',
                          position=0,
                          leave=False):
            def closure():
                if torch.is_grad_enabled():
                    optimizer.zero_grad()
                charge_tmp = Charge2D(dev=dev)
                for i in range(charge_k_demi_init.N):
                    curve_tmp = Curve2D(a_param[i], charge_k_demi_init[i].x)
                    charge_tmp = charge_tmp + Charge2D(np.array([curve_tmp]))
                outputs = phi_amplitude_charge_2D(a_param, x_k_flat, dom,
                                                  noyau, dev=dev)
                loss = 0.5 * mse_loss(acquis, outputs)
                loss += regul * charge_tmp.norm_V()
                if loss.requires_grad:
                    loss.backward()
                del outputs
                return loss

            optimizer.step(closure)

        a_k_demi = a_param.detach().clone()  # discard the tree history
        x_k_demi = charge_k_demi_init.curves_positions()
        curve_tmp = Curve2D(a_k_demi[0], charge_k_demi_init[0].x, dev=dev)
        charge_k_demi = Charge2D(np.array([curve_tmp]), dev=dev)
        for i in range(1, charge_k_demi_init.N):
            curve_tmp = Curve2D(a_k_demi[i], charge_k_demi_init[i].x)
            charge_k_demi += Charge2D(np.array([curve_tmp]))

        charge_k_demi = bridgeTheChargeGap(charge_k_demi)
        charge_k_demi.plot(y_dvf, domain,
                           title=str(k)+"_iterate_crude_approximation")
        del a_param, optimizer, mse_loss

        # print('* x_k_demi : ' + str(np.round(x_k_demi, 2)))
        # print('* a_k_demi : ' + str(np.round(a_k_demi, 2)))
        if verbose:
            print(f'* Convex optimisation: {a_k_demi}')
            # print('* Convex optimisation')

        # Non-convex LASSO (step 8)
        param = torch.cat((a_k_demi, x_k_demi.reshape(-1)))
        param.requires_grad = True

        mse_loss = torch.nn.MSELoss(reduction='sum')
        optimizer = torch.optim.Adam([param])
        n_epoch = 200

        for epoch in tqdm(range(n_epoch),
                          desc=f'[AMG] Computing non-convex step {k} on {dev}',
                          position=0,
                          leave=False):
            def closure():
                # global global_list_of_charges
                optimizer.zero_grad()
                x_tmp = param[Nk+1:].reshape(-1, 2)
                charge_tmp = Charge2D(dev=dev)
                counter = 0
                for i in range(charge_k_demi.N):
                    N_pts = charge_k_demi[i].N_approx
                    curve_tmp = Curve2D(param[i],
                                        x_tmp[counter:counter+N_pts, :])
                    charge_tmp = charge_tmp + Charge2D(np.array([curve_tmp]))
                    counter += N_pts
                fidelity = phiCharge(charge_tmp, dom, noyau)
                loss = 0.5 * mse_loss(acquis, fidelity)
                loss += regul * charge_tmp.norm_V()
                loss.backward()
                # global_list_of_charges = global_list_of_charges + [charge_tmp]
                del fidelity, x_tmp, N_pts
                return loss

            optimizer.step(closure)

        a_k_plus = param[:charge_k_demi.N].detach().clone()
        x_k_plus = param[charge_k_demi.N:].reshape(-1, 2).detach().clone()
        charge_k = Charge2D(dev=dev)
        counter = 0
        for i in range(charge_k_demi.N):
            N_pts = charge_k_demi[i].N_approx
            curve_tmp = Curve2D(
                a_k_plus[i], x_k_plus[counter:counter+N_pts, :])
            charge_k += Charge2D(np.array([curve_tmp]))
            counter += N_pts
        charge_k = bridgeTheChargeGap(charge_k)

        # print('* a_k_plus : ' + str(np.round(a_k_plus, 2)))
        # print('* x_k_plus : ' +  str(np.round(x_k_plus, 2)))

        # # Update parameters while pruning weak curves
        # charge_k = charge_k.prune()

        a_k = charge_k.curves_amplitudes()
        x_k = charge_k.curves_positions()
        Nk = charge_k.N
        charge_k.plot(y_dvf, domain,
                      title=str(k)+"_iterate_final")
        if verbose:
            print(f'* Non-convex optimisation: {Nk} curves, {a_k}')

        # Graph and energy
        nrj_array[k] = charge_k.energy(dom, noyau, acquis, regul)
        if verbose:
            print(f'* Energy: {nrj_array[k]:.3e}')
        if mesParIter == True:
            reconstructed_charges = np.append(reconstructed_charges,
                                              [charge_k])
            torch.save(reconstructed_charges,
                       'saved_objects/mes_' + noyau.type + '.pt')
        try:
            if (N_vector[-1] == N_vector[-2]
                and N_vector[-1] == N_vector[-3]
                    and N_vector[-1] == N_vector[-4]):
                if verbose:
                    print('\n[!] Algorithm has optimised')
                    print("\n\n---- End of loop ----")
                if mesParIter:
                    return(charge_k, nrj_array[:k], reconstructed_charges)
                return(charge_k, nrj_array)
        except IndexError:
            pass
        N_vector = np.append(N_vector, Nk)
        del (a_k_plus, x_k_plus, optimizer, mse_loss, x_k_demi, a_k_demi)
        torch.cuda.empty_cache()

    # End of loop over
    if verbose:
        print("\n\n---- End of loop ----")
    if mesParIter:
        return(charge_k, nrj_array, reconstructed_charges)
    return(charge_k, nrj_array)


α = 1e-3
it = 2
(dvf_csfw, nrj_csfw, mes_csfw) = CSFW(grad_y_dvf, domain, psf_grad,
                                      regul=α, nIter=it)
print(f"[AMG] CSFW: reconstructed {dvf_csfw}")
# print(f"[AMG] Energy: {nrj_csfw}")


#%%

# Plot it
plt.figure()
cont1 = plt.contourf(domain_cpu.X, domain_cpu.Y,
                     y_dvf.to('cpu'), 100, cmap='bone')
for c in cont1.collections:
    c.set_edgecolor("face")
plt.colorbar()

dvf_cpu = dvf.to('cpu')
for i in range(dvf_csfw.N):
    mu_dvf_cpu = dvf[i].to('cpu')
    mu_sfw_cpu = dvf_csfw[i].to('cpu')
    if i == 0:
        plt.plot(mu_dvf_cpu.x[:, 0], mu_dvf_cpu.x[:, 1], c='blue',
                 label='Ground-truth')
        plt.plot(mu_sfw_cpu.x[:, 0], mu_sfw_cpu.x[:, 1],
                 'r-', label='Reconstruction')
    else:
        plt.plot(mu_dvf_cpu.x[:, 0], mu_dvf_cpu.x[:, 1], c='blue')
        plt.plot(mu_sfw_cpu.x[:, 0], mu_sfw_cpu.x[:, 1], 'r-')
    plt.scatter(mu_dvf_cpu.x[0, 0], mu_dvf_cpu.x[0, 1], c='blue')
    plt.scatter(mu_dvf_cpu.x[-1, 0], mu_dvf_cpu.x[-1, 1], c='blue')

    plt.scatter(mu_sfw_cpu.x[0, 0], mu_sfw_cpu.x[0, 1], c='red')
    plt.scatter(mu_sfw_cpu.x[-1, 0], mu_sfw_cpu.x[-1, 1], c='red')

plt.axis('off')
plt.xlabel('$x$', fontsize=18)
plt.ylabel('$y$', fontsize=18)
# plt.title('Reconstructed charge', fontsize=18)
plt.axis([0, 1, 0, 1])
plt.axis('square')
plt.legend(loc=2)
# plt.savefig('fig/reconstruction.png', transparent=True, dpi=1000)
plt.savefig("fig/reconstruction.pdf", format='pdf', dpi=1000,
            bbox_inches='tight', pad_inches=0.03)
plt.show()


# # %% HIERHER remove all mention of global_list_of_charges


# def gif_closure(m_list, acquis, dom, step=100, video='mp4', title=None,
#                 log=True):
#     '''Plot the gif on the reconstruction along the iterations
#     GT measure is unknown'''
#     fig = plt.figure(figsize=(10, 10))

#     ax1 = fig.add_subplot(111)
#     ax1.set_aspect('equal', adjustable='box')
#     cont = plt.contourf(domain_cpu.X, domain_cpu.Y, y_dvf.to('cpu'), 100,
#                         cmap='bone')
#     divider = make_axes_locatable(ax1)  # pour paramétrer colorbar
#     cax = divider.append_axes("right", size="5%", pad=0.15)
#     fig.colorbar(cont, cax=cax)
#     ax1.set_xlabel('X', fontsize=25)
#     ax1.set_ylabel('Y', fontsize=25)
#     ax1.set_title(r'Support optimisation$', fontsize=35)

#     plt.tight_layout()

#     def animate(k):
#         if log:
#             print(k)
#         if k >= len(m_list):
#             # On fige l'animation pour faire une pause à la pause
#             return

#         ax1.clear()
#         ax1.set_aspect('equal', adjustable='box')
#         ax1.contourf(domain_cpu.X, domain_cpu.Y, y_dvf.to('cpu'), 100,
#                      cmap='bone')
#         for i in range(m_list[k].N):
#             mu_dvf_cpu = dvf[i].to('cpu')
#             mu_sfw_cpu = m_list[k][i].to('cpu')
#             if i == 0:
#                 ax1.plot(mu_dvf_cpu.x.detach().numpy()[:, 0],
#                          mu_dvf_cpu.x.detach().numpy()[:, 1], c='green',
#                          label='Ground-truth')
#                 ax1.plot(mu_sfw_cpu.x.detach().numpy()[:, 0],
#                          mu_sfw_cpu.x.detach().numpy()
#                          [:, 1],
#                          'r--', label='Reconstruction')
#             else:
#                 ax1.plot(mu_dvf_cpu.x[:, 0], mu_dvf_cpu.x[:, 1], c='green')
#                 ax1.plot(mu_sfw_cpu.x.detach().numpy()[:, 0],
#                          mu_sfw_cpu.x.detach().numpy()[:, 1],
#                          'r--')
#             ax1.scatter(mu_dvf_cpu.x[0, 0], mu_dvf_cpu.x[0, 1], c='green')
#             ax1.scatter(mu_dvf_cpu.x[-1, 0], mu_dvf_cpu.x[-1, 1], c='green')

#             ax1.scatter(mu_sfw_cpu.x.detach().numpy()[0, 0],
#                         mu_sfw_cpu.x.detach().numpy()[0, 1], c='red')
#             ax1.scatter(mu_sfw_cpu.x.detach().numpy()[-1, 0],
#                         mu_sfw_cpu.x.detach().numpy()[-1, 1], c='red')

#         ax1.set_xlabel('X', fontsize=25)
#         ax1.set_ylabel('Y', fontsize=25)
#         ax1.legend()
#         ax1.set_title(f'Support optimisation step {k}', fontsize=35)
#         plt.tight_layout()

#     anim = FuncAnimation(fig, animate, interval=step, frames=len(m_list)+3,
#                          blit=False)

#     plt.draw()

#     if title is None:
#         title = 'fig/support_optimisation'
#     elif isinstance(title, str):
#         title = 'fig/' + title
#     else:
#         raise TypeError("You ought to give a str type name for the video file")

#     if video == "mp4":
#         anim.save(title + '.mp4')
#     elif video == "gif":
#         anim.save(title + '.gif')
#     else:
#         raise ValueError('Unknown video format')
#     return fig


# gif_closure(global_list_of_charges[::2], y_dvf.to('cpu'), domain)
# gif_closure(global_list_of_charges[::20], y_dvf.to('cpu'), domain)

# %% Plot plot support

y_0 = dvf[0].kernel(domain, psf)
grad_y = dvf[0].kernel(domain, psf_grad)

alpha = 1
eta_alpha_2 = etakCurve(mu_1, 0, domain, psf_grad, alpha) + \
    etakCurve(mu_2, 0, domain, psf_grad, alpha)

eta_alpha = etakCurve(mu_1, 0, domain, psf_grad, alpha) + \
    etakCurve(mu_2, 0, domain, psf_grad, alpha)

# Laplacian of the curve
fig, axes = plt.subplots(nrows=1, ncols=2, figsize=(12, 5))
im1 = axes.flat[0].imshow(eta_alpha[0].T.to('cpu'), origin='lower',
                          cmap='bone')
axes.flat[0].title.set_text(r'$(\Phi^\ast y)^{(1)}$')
axes.flat[0].scatter((N_ECH-1)*dvf_cpu[0].x[0, 0], (N_ECH-1)*dvf_cpu[0].x[0, 1],
                      c='red')
axes.flat[0].scatter((N_ECH-1)*dvf_cpu[0].x[-1, 0], (N_ECH-1)*dvf_cpu[0].x[-1, 1],
                      c='red')
axes.flat[0].plot((N_ECH-1)*(dvf_cpu[0].x[:, 0]), (N_ECH-1)*(dvf_cpu[0].x[:, 1]),
                  c='red', label='$\Gamma=\gamma([0,1])$')

im2 = axes.flat[1].imshow(eta_alpha[1].T.to('cpu'), origin='lower',
                          cmap='bone')
axes.flat[1].title.set_text(r'$(\Phi^\ast y)^{(2)}$')
axes.flat[1].scatter((N_ECH-1)*dvf_cpu[0].x[0, 0],
                      (N_ECH-1)*dvf_cpu[0].x[0, 1], c='red')
axes.flat[1].scatter((N_ECH-1)*dvf_cpu[0].x[-1, 0],
                      (N_ECH-1)*dvf_cpu[0].x[-1, 1], c='red')
axes.flat[1].plot((N_ECH-1)*(dvf_cpu[0].x[:, 0]), (N_ECH-1)*(dvf_cpu[0].x[:, 1]),
                  c='red', label='$\Gamma=\gamma([0,1])$')

axes[0].axis('off')
axes[1].axis('off')
axes[0].legend()
axes[1].legend()

fig.subplots_adjust(right=0.78)
cbar_ax = fig.add_axes([0.85, 0.15, 0.03, 0.7])
fig.colorbar(im1, cax=cbar_ax)
plt.show()

# Sum
lapl = eta_alpha[0].T + eta_alpha[1].T
plt.imshow(lapl, origin='lower', cmap='bone')
plt.colorbar()
plt.scatter((N_ECH-1)*dvf_cpu[0].x[0, 0], (N_ECH-1)*dvf_cpu[0].x[0, 1], c='red')
plt.scatter((N_ECH-1)*dvf_cpu[0].x[-1, 0], (N_ECH-1)*dvf_cpu[0].x[-1, 1], c='red')
plt.plot((N_ECH-1)*(dvf_cpu[0].x[:, 0]), (N_ECH-1)*(dvf_cpu[0].x[:, 1]),
          c='red', label='$\Gamma=\gamma([0,1])$')
plt.axis('off')
plt.legend()
plt.savefig("fig/lapl_sum.pdf", format='pdf', dpi=1000, bbox_inches='tight',
            pad_inches=0.03)
plt.show()


# Threshold
# arbitrary threshold at this point
lapl_bool = lapl.to('cpu') < lapl.min() * 60/100
plt.imshow(lapl_bool, origin='lower', cmap='bone')
plt.scatter((N_ECH-1)*dvf_cpu[0].x[0, 0], (N_ECH-1)*dvf_cpu[0].x[0, 1], c='red')
plt.scatter((N_ECH-1)*dvf_cpu[0].x[-1, 0], (N_ECH-1)*dvf_cpu[0].x[-1, 1], c='red')
plt.plot((N_ECH-1)*(dvf_cpu[0].x[:, 0]), (N_ECH-1)*(dvf_cpu[0].x[:, 1]),
          c='red', label='$\Gamma=\gamma([0,1])$')
plt.axis('off')
plt.legend()
plt.savefig("fig/support.pdf", format='pdf', dpi=1000, bbox_inches='tight',
            pad_inches=0.03)
plt.show()

gamma_curve = torch.stack((domain.X[lapl_bool], domain.Y[lapl_bool]), dim=1)
gamma_curve = gamma_curve.numpy()

# Complex angle
complex_gamma_curve = (gamma_curve - gamma_curve.mean(0))
gamma_curve_sorted = gamma_curve[np.angle(
    (complex_gamma_curve[:, 0] + 1j*complex_gamma_curve[:, 1])).argsort()]

tck, u = splprep(gamma_curve_sorted.T, u=None, s=0.0, per=1, k=1)
u_new = np.linspace(u.min(), u.max(), 1000)
x_new, y_new = splev(u_new, tck, der=0)

plt.figure()
plt.plot(gamma_curve[:, 0], gamma_curve[:, 1], 'rx', label='Pixels')
plt.plot(x_new, y_new, 'b--', label='Fitted curve')
plt.axis('square')
plt.legend()
plt.show()




#%% Datzumin time time
__toc__ = time.time()
__elapsed_time__ = time.gmtime(__toc__ - __tic__)
elapsed_str = ""
if __elapsed_time__.tm_hour != 0:
    elapsed_str += str(__elapsed_time__.tm_hour) + "h "
if __elapsed_time__.tm_min != 0:
    elapsed_str += str(__elapsed_time__.tm_min) + "m "
if __elapsed_time__.tm_sec != 0:
    elapsed_str += str(__elapsed_time__.tm_sec) + "s "

print(f"[AMG] Elapsed time: {elapsed_str}")
# print(f"[AMG] Elapsed time: {__toc__ - __tic__:.1f}s")
